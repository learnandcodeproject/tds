const expect = require('chai').expect;
import {TDSSerializerFactory} from "./service/TDSSerializerFactory";
import * as testData from "./serializationTest.json";
let TDSResponse: any = TDSSerializerFactory.Instance.getDependancy("TDSResponse");
let responseSerializer: any = TDSSerializerFactory.Instance.getDependancy("ResponseSerializer");

describe("Response data Serialization ", function () {
    it("Should serialize the data Object", function () {
        TDSResponse.Headers = {
            "status": "SUCCESS",
            "node-id": "12345",
            "Error-code": "200",
            "Error-message": "message"
        }
        TDSResponse.protocolType = "response";
        let serialize_data = responseSerializer.serialize(TDSResponse);
        expect(serialize_data).equals(JSON.stringify(testData.Response));
    });

    it("Deserialize the data Object ", async function () {
        let deSerialize_data = responseSerializer.deSerialize(JSON.stringify(testData.Response));
        expect(deSerialize_data.protocolType).equals(testData.Response.protocolType);
        expect(JSON.stringify(deSerialize_data.headers)).equals(JSON.stringify(testData.Response.headers));
    });
});