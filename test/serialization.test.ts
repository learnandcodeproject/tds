const expect = require('chai').expect;
import {TDSSerializerFactory} from "./service/TDSSerializerFactory";
import * as testData from "./serializationTest.json";
let TDSRequest: any = TDSSerializerFactory.Instance.getDependancy("TDSRequest");
let requestSerializer: any = TDSSerializerFactory.Instance.getDependancy("RequestSerializer");

describe("Request data Serialization ", function () {
    it("Should serialize the data Object", function () {
        TDSRequest.Headers = {
            "method": "node-add",
            "node-name": "node1",
            "node-ip": "ip-address",
            "node-port": "port"
        }
        TDSRequest.protocolType = "request";
        let serialize_data = requestSerializer.serialize(TDSRequest);
        expect(serialize_data).equals(JSON.stringify(testData.Request));
    });

    it("Deserialize the data Object ", async function () {
        let deSerialize_data = requestSerializer.deSerialize(JSON.stringify(testData.Request));
        expect(deSerialize_data.protocolType).equals(testData.Request.protocolType);
        expect(JSON.stringify(deSerialize_data.headers)).equals(JSON.stringify(testData.Request.headers));
    });
});