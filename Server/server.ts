var net = require('net');

const server = net.createServer((connection:any) => {

    connection.on('data', (data: string) => {
        connection.write(data);
    });

    connection.on('end', () => {
        console.log('client left');
    })
});

server.listen(9090);