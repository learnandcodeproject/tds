var net = require('net');

const options = { //server ip and port
    port: 8888
};

const client = net.createConnection(options, () => {
    client.write('hello');
});

client.on('data', (data:any) => {
    console.log(data.toString());
    client.end();
});