export class TdsProtocol {
    private sourceIp!: string; //client
    private destIp!: string;
    private sourcePort!: number;
    private destPort!: number;
    private payload!: JSON;
    private headers!: {
        protocolVersion: string;
        protocolFormat: string;
    };
    private protocolType!: string;

    constructor(data: any) {
        this.headers.protocolVersion = "2.1";
        this.headers.protocolFormat = "json";
        this.protocolType = data.protocolType;
        this.headers = data.headers;
    }

    get sourceIP() {
        return this.sourceIp;
    }

    set sourceIP(sourceIp: string) {
        this.sourceIp = this.sourceIp;
    }

    get destIP() {
        return this.destIp;
    }

    set destIP(destIp: string) {
        this.destIp = this.destIp;
    }

    get sourceport() {
        return this.sourcePort;
    }

    set sourceport(sourcePort: number) {
        this.sourcePort = this.sourcePort;
    }

    get destport() {
        return this.destPort;
    }

    set destport(destPort: number) {
        this.destPort = this.destPort;
    }

}