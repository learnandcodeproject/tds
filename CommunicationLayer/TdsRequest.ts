import { TdsProtocol } from "./TdsProtocol";

export class TdsRequest extends TdsProtocol {
    Headers: any;
    constructor(request:any){
        super(request);
    }

    getMethod():string {
        return this.Headers;
    }

    setMethod(method:string){
        this.Headers = method;
    }

    getParameter(key:string):string {
        return this.Headers[key];
    }

    addParameter(key:string, value:string) {
        this.Headers[key] = value;
    }
}
