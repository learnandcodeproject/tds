import { serializer } from "../serializer/interface/serialInterface";
import { TdsRequest } from "../TdsRequest";

export class requestSerializer implements serializer {
    serialize(call: TdsRequest): string {
        return JSON.stringify(call);
    }
    deSerialize(data: string): TdsRequest {
        return new TdsRequest(JSON.parse(data));
    }
} 