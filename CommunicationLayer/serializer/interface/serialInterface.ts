import { TdsProtocol } from "../../TdsProtocol"
export interface serializer {
    serialize(call:TdsProtocol):string;
    deSerialize(data:any):TdsProtocol;
}


// serialize(convert to string) & acknowledge to client through server