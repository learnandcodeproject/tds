import { TdsResponse } from "../TdsResponse";
import { serializer } from "./interface/serialInterface";

export class dataSerializer implements serializer {
    serialize(res:TdsResponse):string {
        const serialize_data = JSON.stringify(res);
        return serialize_data;
    }
    deSerialize(data:any) {
        const deSerialize_data = JSON.parse(data);
        return deSerialize_data;
    }
} 