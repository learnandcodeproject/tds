import { TdsProtocol } from "./TdsProtocol";

export class TdsResponse extends TdsProtocol {
    Headers: any;
    constructor(response: any) {
        super(response);
    }

    getStatus(): boolean {
        return this.Headers["status"];

    }
    getErrorCode(): number {
        return this.Headers["Error-code"];
    }
    getErrorMessage(): number {
        return this.Headers["Error-message"];
    }
    getValue(key: string): string {
        return this.Headers[key];
    }
}