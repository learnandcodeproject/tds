const mysql = require('mysql');
import { db } from '../../Server/createFactory';
import { daoInterface } from '../../DbLayer/Interface/DaoInterface';
import { nodeInterface } from '../../DbLayer/Interface/nodeInterface';

export class NodeDAO implements daoInterface {
    config: any;
    connection: any;
    constructor() {
        this.config = db.connectionFactory();
        this.connection;
    }

    Create(req: nodeInterface) {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('INSERT INTO nodes SET ?', (error: any, result: any) => {
            try {
                console.log('success', 'Data added successfully!');
            }
            catch (error) {
                console.log('error', error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Update(req: nodeInterface) {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('UPDATE nodes SET ? WHERE id = ' + req.nodeid, (error: any, result: any) => {
            try {
                console.log('success', 'Data updated successfully!');
                console.log('node/edit', {
                    nodeid: req.nodeid,
                    nodeport: req.nodeport,
                    nodeip: req.nodeip,
                    state: req.state,
                    result: req.result
                })
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Read() {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('select * from nodes', (error: any, results: any) => {
            try {
                console.log("All Nodes data:", results);
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Delete(req: nodeInterface) {
        this.connection = mysql.createConnection(this.config);
        var user = { id: req.nodeid };
        this.connection.query('DELETE FROM nodes WHERE id = ' + req.nodeid, (error: any, result: any) => {
            try {
                console.log("Selected Node is deleted:", result);
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }
}