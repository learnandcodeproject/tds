const mysql = require('mysql');
import { db } from '../../Server/createFactory';
import { daoInterface } from '../../DbLayer/Interface/DaoInterface';
import { clientInterface } from '../../DbLayer/Interface/clientInterface';

export class ClientDAO implements daoInterface {
    config: any;
    connection: any;
    constructor() {
        this.config = db.connectionFactory();
        this.connection;
    }

    Create(req: clientInterface) {
        this.connection = mysql.createConnection(this.config);
        console.log("-----------------inside db------------>>>>>>>");
        this.connection.query('insert into clients (`userid`,`userName`,`hostname`, `password`) VALUES (2221, "tylor34",12342678)', (error: any, result: any) => {
        // this.connection.query('insert into clients = ', + req, (error: any, result: any) => {
            console.log("outside try -----------------------");
            
            try {
                console.log('success', 'Data added successfully!');
            }
            catch (error) {
                console.log('error', error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Update(req: clientInterface) {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('UPDATE clients SET ? WHERE id = ' + req.userid, (error: any, result: any) => {
            try {
                console.log('success', 'Data updated successfully!');
                console.log('client/edit', {
                    userId: req.userid,
                    userName: req.userName,
                    hostname: req.hostName,
                    password: req.passWord
                })
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Read() {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('select * from clients', (error: any, results: any) => {
            try {
                console.log(results);
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }


    Delete(req: clientInterface) {
        var user = { id: req.userid };
        this.connection = mysql.createConnection(this.config);
        this.connection.query('DELETE FROM clients WHERE id = ' + req.userid, (error: any, result: any) => {
            try {
                console.log("Selected Node is deleted:", result);
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }
}