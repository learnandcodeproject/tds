export interface nodeInterface {
    nodeid: string;
    nodeport: string;
    nodeip: string;
    state: string;
    result: string;
}