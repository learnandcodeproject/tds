export interface taskInterface {
    taskid: string;
    tasktype: string;
    taskpath: string;
    hostName: string;
    passWord: string;
}