How to insert Update Delete Stored Procedure in SQL Server

select * from nodes

CREATE PROCEDURE nodes 
(
   @nodeid INT
   @nodeport VARCHAR(30)
   @nodeip VARCHAR(30)
   @state VARCHAR(20)
   @result VARCHAR(20)
   @stmnt VARCHAR(20)=''
)
AS
BEGIN
IF @Stmnt = 'insert'
BEGIN
INSERT INTO nodes (nodeid,nodeport,nodeip,state,result) VALUES (@nodeid,@nodeport,@nodeip,@state,@result)
END

IF @Stmnt = 'select'
Begin
select * from nodes 
End 


IF @Stmnt = 'update'
BEGIN
Update nodes set nodeid=@nodeid, nodeport=@nodeport, nodeip=@nodeip, state=@state, result=@result
where nodeid =@nodeid
end

else if @Stmnt = 'delete'
Begin
Delete from nodes where nodeid=@nodeid
End
End

