CREATE DATABASE TDS CHARACTER SET utf8 COLLATE utf8_general_ci;
USE TDS;

CREATE TABLE clients (
  userid INT(10),
  userName VARCHAR(30),
  hostName VARCHAR(30),
  passWord VARCHAR(30)
) ;

CREATE TABLE tasks (
taskid INT(10),
hostName VARCHAR(30),
tasktype VARCHAR(30),
taskpath VARCHAR(30)
);

CREATE TABLE nodes (
 nodeid INT(10),
 nodeport VARCHAR(10),
 nodeip VARCHAR(50),
 state  VARCHAR(30),
 result VARCHAR(100)
) ;

