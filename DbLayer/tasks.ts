const mysql = require('mysql');
import { db } from '../Server/createFactory';
import { daoInterface } from '../Utility/DaoInterface';
import { taskInterface } from '../Utility/taskInterface';

export class TaskDAO implements daoInterface {
    config: any;
    connection: any;
    constructor() {
        this.config = db.connectionFactory();
        this.connection;
    }

    Create(req: taskInterface) {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('INSERT INTO tasks SET ?' + req, (error: any, result: any) => {
            try {
                console.log('success', 'Data added successfully!');
            }
            catch (error) {
                console.log('error', error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Update(req: taskInterface) {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('UPDATE tasks SET ? WHERE id = ' + req.taskid, (error: any, result: any) => {
            try {
                console.log('success', 'Data updated successfully!');
                console.log('task/edit', {
                    taskid: req.taskid,
                    tasktype: req.tasktype,
                    taskpath: req.taskpath,
                    hostname: req.hostName,
                    passWord: req.passWord
                });
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Read() {
        this.connection = mysql.createConnection(this.config);
        this.connection.query('select * from tasks', (error: any, results: any) => {
            try {
                console.log(results);
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }

    Delete(req: taskInterface) {
        this.connection = mysql.createConnection(this.config);
        var user: any = { id: req.taskid };
        this.connection.query('DELETE FROM tasks WHERE id = ' + req.taskid, (error: any, result: any) => {
            try {
                console.log("Selected task is deleted:", result);
            }
            catch (error) {
                console.log(error);
            }
            finally {
                this.connection.end;
            }
        });
    }
}